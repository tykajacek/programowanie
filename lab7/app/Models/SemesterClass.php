<?php

namespace Scheduler\Models;

use Scheduler\Interfaces\SemesterClassInterface;

abstract class SemesterClass implements SemesterClassInterface {
	
	private $name;
	private $day;
	private $hour;
	private $teacher;
	private $classNumber;
	
	/*public function __construct(string $name, int $day, int $hour, string $teacher, string $classNumber){
		$this->name = $name;
		$this->day = $day;
		$this->hour = $hour;
		$this->teacher = $teacher;
		$this->classNumber = $classNumber;
	}*/
	public function __construct() {
	
	}
	public function setName(string $name) {
		$this->name=$name;
		
	}
	public function getName(): string {
		return $this->name;
	}
	
	public function setDay(string $day) {
		switch ($day) {
			case "Poniedziałek":
				{$this->day = 1;
				break;}
				
			case "Wtorek":
				{$this->day = 2;
				break;}
			
			case "Środa":
				{$this->day = 3;
				break;}
			
			case "Czwartek":
				{$this->day = 4;
				break;}
			
			case "Piątek":
				{$this->day = 5;
				break;}
				
			default: break;
		}
	}
	
	public function setHour(int $hour) {
		$this->hour = $hour;
	}
				
	
	public function setTeacher(string $teacher) {
		$this->teacher = $teacher;
	}
	
	
	public function setClassNumber(string $classNumber) {
		$this->classNumber = $classNumber;
	}	
				
	public function getClassNumber(): string {
		return $this->classNumber;
	}		
	
	public function getTeacher(): string {
		return $this->teacher;
	}

	public function getDay(): int{
		return $this->day;
	}
	
	public function getHour(): int{
		return $this->hour;
	}

}				
