<?php

namespace Scheduler\Interfaces;

interface SemesterClassInterface {

	public function getName(): string;
	public function setName(string $name);
	public function getFormName(): string;
	public function setDay(string $day);
	public function setHour(int $hour);
	public function setTeacher(string $teacher);
	public function setClassNumber(string $classNumber);
	
	

}
