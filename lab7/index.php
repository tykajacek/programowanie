//KULESZA PAWEŁ GR.2(1) 38289

<?php

require "./vendor/autoload.php";

use Scheduler\Models\Lecture;
use Scheduler\Models\Schedule;
use Scheduler\Models\Seminar;
use Scheduler\Models\PhysicalEducations;
use Scheduler\Models\Laboratory;
use Scheduler\Models\Exercises;
use Scheduler\Models\SpecialLesson;
use Scheduler\Models\Project;

$twig = new Twig_Environment(new Twig_Loader_Filesystem("/"), []);

$schedule = new Schedule();


//$schedule->save(new Lecture("Bazy danych"), 1, 1);
//$schedule->save(new Lecture("Programowanie i projektowanie obiektowe"), 1, 2);

$ClassTeachers = [];
$ClassTeachers[0] = "Selwat";
$ClassTeachers[8] = "Selwat";
$ClassTeachers[1] = "Klosow";
$ClassTeachers[9] = "Fryzlewicz";
$ClassTeachers[2] = "Kendziorczyk-Twardoch";
$ClassTeachers[3] = "Nadybski";
$ClassTeachers[10] = "Nadybski";
$ClassTeachers[4] = "Klosow";
$ClassTeachers[5] = "Duda";
$ClassTeachers[11] = "Kordecki";
$ClassTeachers[6] = "Sadownik";
$ClassTeachers[7] = "Fryzlewicz";

$ClassDays = Array("Środa","Poniedziałek","Poniedziałek","Poniedziałek","Piątek","Środa","Czwartek","Piątek","Środa","Poniedziałek","Czwartek","Czwartek");

$ClassHours = Array(1,2,3,4,3,2,5,4,3,5,3,4);

$ClassNumbers = Array("C212","C225","C104","A231","A21","C10","C11","C225","C104","C212","C11","C212");

$ClassNames = [];
$ClassNames[0] = "Matematyka"; //wyk i lab
$ClassNames[8] = "Matematyka"; //wyk i lab
$ClassNames[1] = "Projektowanie i programowanie obiektowe"; //wyk i lab
$ClassNames[9] = "Projektowanie i programowanie obiektowe"; //wyk i lab
$ClassNames[2] = "Język angielski"; //ćw
$ClassNames[3] = "Sieci komputerowe"; //wyk i lab
$ClassNames[10] = "Sieci komputerowe"; //wyk i lab
$ClassNames[4] = "Seminarium dyplomowe"; //sem
$ClassNames[5] = "Architektura komputerów"; //wyk i lab
$ClassNames[11] = "Architektura komputerów"; //wyk i lab
$ClassNames[6] = "Wychowanie Fizyczne" ;//wf
$ClassNames[7] = "Projekt zespołowy" ; //projekt




for($i=0; $i < 12; $i++) {
	if(($i<=1)||($i==3)||($i==11)){
		$lesson = new Lecture();
			$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
			$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	}
	
	if(($i==2)||($i==8)){
	$lesson = new Exercises();
			$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
			$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	}
	
	if($i==4){
	$lesson = new Seminar();
			$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
		$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	}
	
	if(($i==5)||($i==9)||($i==10)){
		$lesson = new Laboratory();
		$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
			$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	
	}
	
	if($i==6){
		$lesson = new PhysicalEducations();
			$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
			$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	}
	
	if($i==7){
		$lesson = new Project();
		$lesson->setName($ClassNames[$i]);
			$lesson->setDay($ClassDays[$i]);
			$lesson->setHour($ClassHours[$i]);
			$lesson->setTeacher($ClassTeachers[$i]);
			$lesson->setClassNumber($ClassNumbers[$i]);
		$hour = $lesson-> getHour();
			$day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
	}

}

/*$lesson = new SpecialLesson("Liga Akademicka");
$lesson->setDay('Piątek');
			$lesson->setHour(5);
			$lesson->setClassNumber("E2");
		(int)$hour = $lesson-> getHour();
			(int) $day = $lesson-> getDay();
			$schedule->save($lesson, $day, $hour);
*/
			
			
echo $twig->render("index.twig", [
    "schedule" => $schedule,
]);
