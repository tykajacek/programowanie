#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>

//								zrobi� walidatory, rok na rzymski, �adne menu,
using namespace std;

class Student {
	private:
		int studentNo;
		string imie;
		string nazwisko;
		string pesel;
		string adres;
		int rok;
		
	public:		
		Student() {
			int studentNo;
			cout << "Podaj numer albumu: ";
			cin >> studentNo;
			this->studentNo = studentNo;
		}
		int cyfra(int c){
		return c-48;
		}
		void setName() {
			string imie;
			cout << "Podaj imie: ";
			cin >> imie;
			this->imie = imie;
		}
		
		void setSurname() {
			string nazwisko;
			cout << "Podaj nazwisko: ";
			cin >> nazwisko;
			this->nazwisko = nazwisko;
		}
		
	bool spesel(string pesel){
	int DNI_MIESIACA[] = {31,29,31,30,31,30,31,31,30,31,30,31};
	if(pesel.size()!=11)
		return true;
	if(cyfra(pesel[10]) != ((10-(cyfra(pesel[0])*1 + cyfra(pesel[1])*3 + cyfra(pesel[2])*7 +cyfra(pesel[3])*9 + cyfra(pesel[4])*1 + cyfra(pesel[5])*3 + cyfra(pesel[6])*7 + cyfra(pesel[7])*9 +cyfra(pesel[8])*1 + cyfra(pesel[9])*3)%10)%10))
		return true;
	int miesiac=(10*cyfra(pesel[2])+cyfra(pesel[3]))%20;
	int dzien = 10*cyfra(pesel[4])+cyfra(pesel[5]);
	if(miesiac > 12)
		return true;
	if(dzien > DNI_MIESIACA[miesiac])
		return true;
	if(miesiac==2 && (10*cyfra(pesel[0])+cyfra(pesel[1]))%4 == 0 && dzien == 29)
		return true;
	return false;
}
			void setPesel(){
			string strpesel;
			int sprawdzacz=0;
			while(sprawdzacz==0)
			{
			cout << "Podaj pesel: ";
			cin >> strpesel;
			spesel(strpesel);
			if(spesel(strpesel)==false)
			{
				sprawdzacz = 1;
				this->pesel = strpesel;
			}
			}
		}
		void setAdres() {
			string adres;
			cout << "Podaj adres: ";
			cin >> adres;
			this->adres = adres;
		}
		void setRok() {
			int rok;
			bool test=false;
			while(test==false)
			{
			cout << "Podaj rok: ";
			cin >> rok;
			if(rok>0 && rok<=5)
			{
			this->rok = rok;
			test=true;
			}}
		}
		void setRok(int r)
		{
			this->rok = r;
		}
		
		
		
		int getStudentNo() {
			cout << " Numer albumu: ";
			return this->studentNo;
		}
		string getimie() {
			cout << " Imie: ";
			return this->imie;
		}
		string getnazwisko() {
			cout << " Nazwisko: ";
			return this->nazwisko;
		}
		string getpesel() {
			cout << " Pesel: ";
			return this->pesel;
		}
		string getAdres() {
			cout << " Adres: ";
			return this->adres;
		}
		string getRok() {
			string strrok;
			cout << " Rok: ";
			switch(rok)
			{
				case 1:
					strrok="I";
					break;
				case 2:
					strrok="II";
					break;
				case 3:
					strrok="III";
					break;
				case 4:
					strrok="IV";
					break;
				case 5:
					strrok="V";
					break;
			}
			return strrok;
		}
		
};


int main(int argc, char** argv) {
	char wybor='1';
	bool ok=true;
	string taknie;
	vector<Student> students;
	while(ok)
	{
		cout << "wybierz : [ 1 ] pokaz liste studentow , [ 2 ] dodaj studenta , [ 3 ] wyjdz z programu" <<endl;
		cin >> wybor;
	
		switch(wybor)
		{
			case '1':
				if(students.size())
				for(int i = 0; i < students.size(); i++) {
				cout <<setw(18)<< students.at(i).getnazwisko();
				cout << " ";
				cout <<setw(15)<< students.at(i).getimie();
				cout << " ";
				cout <<setw(11)<< students.at(i).getpesel();
				cout << " ";
				cout <<setw(20)<< students.at(i).getAdres();
				cout << " ";
				cout <<setw(3)<< students.at(i).getRok();
				cout << " ";
				cout <<setw(8)<< students.at(i).getStudentNo() << endl;
				}
				else
				cout << "Lista jest pusta"	<<endl;
			break;
			
			case '2':
				{
				Student student;
				student.setName();
				student.setSurname();
				student.setPesel();
				taknie = "null";
				while(taknie!="t" && taknie!="n")
				{
				cout << "Chcesz poda� adres? t/n"<<endl;
				cin>>taknie;
				if(taknie=="t")
				student.setAdres();
				}
				taknie = "null";
				while(taknie!="t" && taknie!="n")
				{
				cout << "Chcesz poda� rok? t/n"<<endl;
				cin>>taknie;
				if(taknie=="t")
				student.setRok();
				else if(taknie=="n")
				student.setRok(0);
				}
				students.push_back(student);
			}
				break;
				
			case '3':
				cout << "KONIEC";
				ok=false;
			break;
			default:
				break;
		}
	}
	return 0;
}
