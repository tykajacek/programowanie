#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Point {
	public:
		int x;
		int y;
		
		Point() {}
		
		Point(int x, int y) {
			this->x = x;
			this->y = y;
		}
		
		~Point() {}
		
		void movePoint(int xAxisShift, int yAxisShift) {
			this->x += xAxisShift;
			this->y += yAxisShift;
		}
};

class Square {
	public:
		Point punkt;
		int krawendz;
		
		Square(Point punkt, int krawendz) {
			this->punkt = punkt;
			this->krawendz = krawendz;
		}
		
		~Square(){
			cout << "Square has been deleted, points:" <<endl;
			cout << "x1: " << this->punkt.x << " " << "y1: " << this->punkt.y << endl;
			cout << "x2: " << this->punkt.x+this->krawendz << " " << "y2: " << this->punkt.y << endl;
			cout << "x3: " << this->punkt.x << " " << "y3: " << this->punkt.y+this->krawendz << endl;
			cout << "x4: " << this->punkt.x+this->krawendz << " " << "y4: " << this->punkt.y+this->krawendz << endl;
		}
		

};

int main() {
	srand(time(NULL));
	
	int inputX = 0, inputY = 0;
	for(int i=0;i<5;i++)
	{
	int inputkrawendz = rand() % 10 +1;
	Point p = Point(inputX, inputY);
	Square c = Square(p, inputkrawendz);
	c.punkt.movePoint(rand() % 10, rand() % 10);
}
	return 0;
}

